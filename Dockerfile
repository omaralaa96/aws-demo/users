FROM node:14
WORKDIR /src
COPY . /src
RUN npm install -g typescript@latest
RUN npm install
EXPOSE 3000
ENTRYPOINT ["npm","start"]