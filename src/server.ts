import "reflect-metadata";
import { createExpressServer } from 'routing-controllers';
import { UsersController } from './UsersComponent/userController';
import * as bodyParser from 'body-parser';
require('dotenv').config();

class Server{
    public static serverInstance:Server;
    private server : any;

    public static bootstrap(): Server{
        if(!this.serverInstance){
            this.serverInstance = new Server();
        }
        return this.serverInstance;
    }

    private async createServer(){
        const app = createExpressServer({
            controllers:[
                UsersController
            ]
        });

        app.use(bodyParser.json());

        const port = process.env.PORT || 3000;
        this.server = app.listen(port, () => {
            console.log(`App listening on the port ` + port);
        });
    }

    public constructor(){
        this.createServer();
    }
}

export const server = Server.bootstrap();