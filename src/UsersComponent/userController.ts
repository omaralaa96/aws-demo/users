import { Request, Response } from 'express'; 
import { JsonController, Get, Req, Res } from 'routing-controllers';
import { UsersService } from './userService';

@JsonController("/users")
export class UsersController {

    readonly service = new UsersService();

    @Get("")
    async getAllUsers(@Req() request: Request, @Res() response: Response){
        const res = this.service.getAllUsers();
        response.send(res);
        return response;
    } 
}